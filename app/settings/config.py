# 项目的配置文件
class DefaultConfig:
    # 默认的配置信息
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@127.0.0.1:3306/topnews'
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 是否追踪数据变化
    SQLALCHEMY_ECHO = False  # 是否打印底层执行的SQL

    # redis配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    # 容联云相关的信息
    ACC_ID = '2c94811c85c276590185fba1c42a03ef'
    ACC_TOKEN = '93a0cccaed0943819b7cabdf05781a37'
    APP_ID = '2c94811c85c276590185fba1c4f903f6'

    # 项目中使用的加密的密钥
    JWT_SECRET = 't!29-9k&o6ph&gv@pr_-9i%^*0yne4hev*e5wl#)=l)#l4yglo'
    # jwt密钥的过期时间
    JWT_EXPIRE_DAYS = 14

    # 七牛云
    ACCESS_KEY = 'JE6AqjyeIFN-iIvTXlNNVzE075GiskOFBfyZFXa_'
    SECRET_KEY = 'R_6d-J3LHAHwzYhTFWHNo5tELdxnPwZ54B9J073M'
    BUCKET_NAME = 'topnews2006a'
    QINIU_DOMAIN = 'http://rql2j1sda.hb-bkt.clouddn.com/'

    # 实名认证 url地址
    AUTH_URL = 'https://zid.market.alicloudapi.com/idcheck/Post'
    APPCODE = '86e9bb4383894a55a295ee32ad5b15fb'

# key:value  key就是配置类的别名，value就是具体的配置类
config_dict = {
    "dev": DefaultConfig
}
