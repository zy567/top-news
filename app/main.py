# Flask项目的程序主入口文件。运行这个文件就可以去启动Flask项目
from app import create_app
from flask import jsonify

# 调用方法创建flask app对象
app = create_app('dev')


# 定义一个测试的接口
@app.route('/')
def index():
    return jsonify({"username": "张三", "mobile": "18771552515"})

