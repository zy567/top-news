# 频道操作
from flask import g, request
from flask_restful import Resource
from sqlalchemy.orm import load_only

from common.models.article import Channel, UserChannel
from common.utils.decorators import login_required
from app import db


# 查询用户的频道
class UserChannelResource(Resource):
    method_decorators = {'put': [login_required]}
    def get(self):
        user_id = g.user_id
        # 用户关注的频道信息
        if user_id:
            # 查询用户的频道
            channels = Channel.query.options(load_only(Channel.id, Channel.name)).join \
                (UserChannel, Channel.id == UserChannel.channel_id).filter(
                UserChannel.user_id == user_id,
                UserChannel.is_deleted == False).order_by(UserChannel.sequence).all()
            # 0 用户没有选择频道, 查询默认频道
            if len(channels) == 0:
                channels = Channel.query.options(load_only(Channel.id, Channel.name)).filter(
                    Channel.is_default == True).all()
        # 未登录的状态 查询所有默认频道
        else:
            channels = Channel.query.options(load_only(Channel.id, Channel.name)).filter(
                Channel.is_default == True).all()

        # 4. 需要将查询到的 频道查询集（包含了一个一个的 频道对象） 转换为 字典（前端需要的数据格式）
        channels_list = [channel.to_dict() for channel in channels]
        # 添加 推荐 频道
        channels_list.insert(0, {'id': 0, 'name': '推荐'})

        return {'channels': channels_list}

    # 修改全部
    def put(self):
        user_id = g.user_id
        channels = request.json.get('channels')
        UserChannel.query.filter(UserChannel.user_id == user_id, UserChannel.is_deleted == False). \
            update({'is_deleted': True})

        # 查询用户是否关注这个频道
        for c in channels:
            data = UserChannel.query.options(load_only(UserChannel.id)). \
                filter(UserChannel.user_id == user_id, UserChannel.channel_id == c['id']).first()
            # 关注 修改序号
            if data:
                data.sequence = c['seq']
                data.is_deleted = False
            # 没关注 添加进去
            else:
                data = UserChannel(user_id=user_id, channel_id=c['id'], sequence=c['seq'])

                db.session.add(data)
        # 提交
        db.session.commit()

        return {"channels": channels}