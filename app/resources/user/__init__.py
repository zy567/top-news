from flask import Blueprint
from flask_restful import Api
from common.utils import constants
from common.utils.output import output_json
from .passport import SMSCodeResource, LoginResource
from .profile import CurrentUserResource, UserPhotoResource, CheckRealNameResource
from .channel import UserChannelResource



# 1. 创建蓝图对象
user_bp = Blueprint('user', __name__, url_prefix=constants.BASE_URL_PRIFIX)

# 2. 创建Api对象
user_api = Api(user_bp)

# 设置json包装格式
user_api.representation('application/json')(output_json)

# 3. 组件去添加类视图
user_api.add_resource(SMSCodeResource, '/sms/codes/<mobile:mobile>')
user_api.add_resource(LoginResource, '/login')
user_api.add_resource(CurrentUserResource, '/user')
user_api.add_resource(UserPhotoResource, '/user/photo')
user_api.add_resource(UserChannelResource, '/user/channels')
user_api.add_resource(CheckRealNameResource, '/user/auth')
