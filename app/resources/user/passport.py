import re
import random

from flask import request, current_app
from datetime import datetime, timedelta
from flask_restful import Resource

from app import redis_client, db
from common.utils.send_sms_code import SendSMSCode
from common.models.user import User
from common.utils.jwt_util import generate_jwt
from sqlalchemy.orm import load_only
from celery_tasks.sms.tasks import send_sms_code
from common.utils.logger import create_logger


logger = create_logger()

# TODO: 发送短信验证码
# 在此函数中去实现发送短信验证码的业务逻辑
class SMSCodeResource(Resource):
    def get(self, mobile):
        # 1. 生成一个随机的六位的数字的验证码
        code = '%06d' % random.randint(0, 999999)

        redis_flay = f'app:code:{mobile}1'
        redis_key = f'app:code:{mobile}'

        redis_node = redis_client.get(redis_flay)
        # 判断 redis 节点 是否拥有
        if redis_node:
            return {'msg': '短信发送频繁'}
        # 异步执行
        send_sms_code.delay(mobile, code)
        # 添加节点
        redis_client.setex(redis_flay, 60, 1)
        # 添加验证码
        redis_client.setex(redis_key, 60, code)

        logger.info(f'对{mobile} 发送了短信验证码{code}')

        return {"code": code}


# TODO: 实现登录和注册的视图
class LoginResource(Resource):

    def post(self):
        # 1. 获取前端传递来的数据 json数据
        mobile = request.json.get('mobile')
        code = request.json.get('code')
        print(code)

        # 2. 数据校验  判断数据完整性
        if not all([mobile, code]):
            return {'msg': '缺失参数, 参数不能为空'}
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return {'msg': '手机号格式错误'}

        redis_key = f'app:code:{mobile}'
        # 验证码
        redis_code = redis_client.get(redis_key)
        if not redis_code or code != redis_code:
            return {"msg": '验证码错误'}
        # 验证后删除短信验证码
        redis_client.delete(redis_key)
        # 3. 处理： 根据手机号 从数据库中查询一下，这个手机号是否存在
        # 3.1 手机号是存在的：验证用户输入的短信验证码是否正确，如果正确，登录成功，状态保持（生成jwt） 验证码错误：登录失败
        user = User.query.options(load_only(User.id)).filter_by(mobile=mobile).first()
        # 3.2 手机号是不存在：验证用户输入的短信验证码是否正确，如果正确，将用户的信息保存到数据库（注册） 登录 状态保持。
        if not user:
            # 手机号保存到user模型类中
            user = User(mobile=mobile, name=mobile, last_login=datetime.now())
            db.session.add(user)
        db.session.commit()
        print(11111, user.id)
        # 获取jwt token
        token = generate_jwt({'user_id': user.id},
                             datetime.utcnow() + timedelta(days=current_app.config['JWT_EXPIRE_DAYS'])).decode()

        return {'token': token}

