from flask import g
from flask_restful import Resource
from flask_restful.inputs import regex
from sqlalchemy.orm import load_only
from flask_restful.reqparse import RequestParser

from app import db
from common.utils.decorators import login_required
from common.models.user import User
from common.utils.img_storage import upload_file
from common.utils.parser import image
from common.utils.real_name_auth import check_real_name


class CheckRealNameResource(Resource):
    def post(self):
        # 获取前端传递的数据
        parser = RequestParser()
        parser.add_argument('name', required=True, location='json', type=str)
        parser.add_argument('card', required=True, location='json', type=regex(r'^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$'))
        # 获取到文件对象
        args = parser.parse_args()
        name = args.name
        card = args.card

        # 实名认证
        flag = check_real_name(name, card)
        if flag:
            return {'msg': '认证成功'}
        else:
            return {'msg': '认证失败'}


class CurrentUserResource(Resource):
    """实现返回用户个人中心 用户信息"""
    # 给get请求添加装饰器
    method_decorators = {'get': [login_required], 'patch': [login_required]}

    def get(self):
        # 只要能够执行到这个get函数里面，表示 用户是登录的状态了
        # 1. 获取登录用户的id
        user_id = g.user_id
        # 2. 根据用户id 从User模型类中去查询到对应的用户对象
        user = User.query.options(
            load_only(User.id, User.name, User.profile_photo, User.introduction, User.article_count,
                      User.following_count, User.fans_count)).filter(User.id == user_id).first()
        # 返回当前登录用户的信息
        return user.to_dict()

    def patch(self):
        # 获取前端传递来的参数
        reqpar = RequestParser()
        reqpar.add_argument('birthday', required=True, location='json', type=str)
        reqpar.add_argument('gender', required=True, location='json', type=str)
        # 获取到文件对象
        args = reqpar.parse_args()
        birthday = args.birthday
        gender = args.gender
        # 获取当前用户对象
        user_id = g.user_id

        User.query.filter(User.id == user_id).update({'birthday': birthday, 'gender': gender})

        db.session.commit()

        return {'gender': gender, 'birthday': birthday}


# 上传用户头像
class UserPhotoResource(Resource):
    method_decorators = {'patch': [login_required]}

    def patch(self):
        # 1. 获取到上传的文件
        # 创建请求解析器对象
        parser = RequestParser()
        parser.add_argument('photo', required=True, type=image, location='files')
        # 获取到文件对象
        args = parser.parse_args()
        img_file = args.photo

        # 读取上传的图片中的bytes类型数据
        img_bytes = img_file.read()

        # 上传文件
        try:
            image_url = upload_file(img_bytes)
        except Exception as e:
            return {'message': 'Thired Error: %s' % e, 'data': None}

        # 如果是正常上传成功之后，将user对象的 photo的值 修改为获取到的图片的url地址
        user_id = g.user_id
        User.query.filter(User.id == user_id).update({'profile_photo': image_url})
        db.session.commit()

        return {'photo_url': image_url}
