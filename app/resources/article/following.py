from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from sqlalchemy.orm import load_only
from datetime import datetime

from flask import g
from app import db
from common.utils.decorators import login_required
from common.models.user import Relation, User


# 用户关注
class FollowUserResource(Resource):
    method_decorators = {'post': [login_required], 'get': [login_required]}
    # 用户关注
    def post(self):
        # 获取用户id
        user_id = g.user_id
        parser = RequestParser()
        parser.add_argument('target', required=True, location='json', type=int)

        args = parser.parse_args()
        author_id = args.target

        # 查询关注状态
        relation = Relation.query.options(load_only(Relation.id, Relation.update_time)). \
            filter(Relation.user_id == user_id, Relation.author_id == author_id).first()

        # 有关系就修改 relation 为 follow 1
        if relation:
            relation.relation = Relation.RELATION.FOLLOW
            relation.update_time = datetime.now()
        # 否则 就新增一条关注信息
        else:
            relation = Relation(user_id=user_id, author_id=author_id, update_time=datetime.now(),
                                relation=Relation.RELATION.FOLLOW)
            db.session.add(relation)
        print(222, relation)

        # 用户关注人数 + 1
        count = Relation.query.filter(Relation.user_id == user_id,
                                      Relation.relation == Relation.RELATION.FOLLOW).count()

        User.query.filter(User.id == user_id).update({'following_count': count})
        # 作者粉丝数 + 1
        User.query.filter(User.id == author_id).update({'fans_count': User.fans_count + 1})

        # 提交
        db.session.commit()

        return {'target': author_id}

    # 互相关注
    def get(self):
        # 获取用户id
        user_id = g.user_id

        reqpar = RequestParser()
        reqpar.add_argument('page', required=True, location='args', type=int)
        reqpar.add_argument('per_page', required=True, location='args', type=int)
        # 解析 返回字典
        args = reqpar.parse_args()
        page = args.page
        per_page = args.per_page

        # 查询用户关注列表
        data = db.session.query(Relation.author_id, User.name, User.fans_count, User.profile_photo). \
            join(Relation, Relation.author_id == User.id).filter(
            Relation.user_id == user_id, Relation.relation == Relation.RELATION.FOLLOW).all()

        print(data)

        # 序列化
        list = []
        for d in data:
            list.append({
                'id': d.author_id,
                'name': d.name,
                'photo': d.profile_photo,
                'fans_count': d.fans_count,
                'mutual_follow': False,
            })

        each = Relation.query.filter_by(author_id=user_id, relation=Relation.RELATION.FOLLOW).all()

        # 查询互相关注的用户
        for l in list:
            for k in each:
                if l['id'] == k.user_id:
                    l['mutual_follow'] = True

        return list


# 取消关注
class UnFollowUserResource(Resource):
    method_decorators = {'delete': [login_required]}

    def delete(self, target):
        user_id = g.user_id

        # 查询关注状态
        Relation.query.filter(Relation.user_id == user_id, Relation.author_id == target,
                              Relation.relation == Relation.RELATION.FOLLOW).first()

        # 用户关注人数 - 1
        User.query.filter(User.id == user_id).update({'following_count': User.following_count - 1})
        # 作者粉丝数 - 1
        User.query.filter(User.id == target).update({'fans_count': User.fans_count - 1})

        # 提交
        db.session.commit()

        return {'target': 'ok'}
