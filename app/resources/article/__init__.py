from flask import Blueprint
from flask_restful import Api
from common.utils import constants
from common.utils.output import output_json

from .channel import AllChannelResource
from .articles import ArticleListResource, ArticleDetaiLResource, SearchArticleResource
from .following import FollowUserResource, UnFollowUserResource
from .comment import CommentResource


# 1. 创建蓝图对象
article_bp = Blueprint('article', __name__, url_prefix=constants.BASE_URL_PRIFIX)

# 2. 创建Api对象
article_api = Api(article_bp)

# 设置json包装格式
article_api.representation('application/json')(output_json)

# 注册路由
article_api.add_resource(AllChannelResource, '/channels')

article_api.add_resource(ArticleListResource, '/articles')

article_api.add_resource(ArticleDetaiLResource, '/articles/<int:article_id>')

article_api.add_resource(FollowUserResource, '/user/followings')

article_api.add_resource(UnFollowUserResource, '/user/unfollowings/<int:target>')

article_api.add_resource(CommentResource, '/comments')

article_api.add_resource(SearchArticleResource, '/search')
