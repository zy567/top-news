from flask_restful import Resource
from flask_restful.inputs import regex
from flask_restful.reqparse import RequestParser
from common.models.article import Comment, Article
from common.models.user import User

from flask import g
from app import db
from common.utils.decorators import login_required


# 获取评论列表
class CommentResource(Resource):
    method_decorators = {'get': [login_required]}

    def post(self):
        user_id = g.user_id
        reqpar = RequestParser()
        reqpar.add_argument('target', required=True, location='json', type=int)
        reqpar.add_argument('content', required=True, location='json', type=regex(r'.+'))
        reqpar.add_argument('parent_id', location='json', type=int)
        # 解析 返回字典
        args = reqpar.parse_args()
        target = args.target
        content = args.content
        parent_id = args.parent_id

        # 判断生成的是评论 还是回复评论
        if parent_id:
            comment = Comment(user_id=user_id, article_id=target, content=content, parent_id=parent_id)
            db.session.add(comment)
            # 对回复评论数量 + 1
            Comment.query.filter(Comment.id == parent_id).update({'reply_count': Comment.reply_count + 1})
        else:
            comment = Comment(user_id=user_id, article_id=target, content=content, parent_id=0)
            db.session.add(comment)
            # 文章的评论数量 + 1
            Article.query.filter(Article.id == target).update({'comment_count': Article.comment_count + 1})
        # 提交
        db.session.commit()

        return {'com_id': comment.id, 'target': target}

    # 获取评论列表
    def get(self):
        reqpar = RequestParser()
        reqpar.add_argument('source', required=True, location='args', type=int)
        reqpar.add_argument('offset', default=0, location='args', type=int)
        reqpar.add_argument('limit', default=10, location='args', type=int)
        reqpar.add_argument('type', required=True, location='args', type=regex(r'[ac]'))

        # 解析 返回字典
        args = reqpar.parse_args()
        source = args.source
        offset = args.offset
        limit = args.limit
        type = args.type

        # 判断是评论列表 还是 回复列表
        if type == 'a':
            # 查询文章的评论列表
            comments = db.session.query(Comment.id, Comment.user_id, User.name, User.profile_photo,
                                        Comment.ctime, Comment.reply_count, Comment.like_count, Comment.content). \
                join(User, Comment.user_id == User.id).filter(Comment.article_id == source, Comment.id > offset). \
                offset(offset).limit(limit).all()
            # 查询某条评论的回复总数
            total = Comment.query.filter(Comment.article_id == source).count()
            # 获取所有评论的最后一条评论的id
            end_comment = db.session.query(Comment.id).filter(Comment.article_id == source). \
                order_by(Comment.id.desc()).first()
            print(11, comments)
        elif type == 'c':
        # 回复列表
        # else:
            # 查询文章的评论列表
            comments = db.session.query(Comment.id, Comment.user_id, User.name, User.profile_photo,
                                        Comment.ctime, Comment.reply_count, Comment.like_count, Comment.content). \
                join(User, Comment.user_id == User.id).filter(Comment.article_id == source, Comment.id > offset). \
                limit(limit).all()

            # 查询某条评论的回复总数
            total = Comment.query.filter(Comment.article_id == source).count()
            # 获取所有评论的最后一条评论的id
            end_comment = db.session.query(Comment.id).filter(Comment.parent_id == source). \
                order_by(Comment.id.desc()).first()
            print(comments)
        # 序列化
        comments_list = [{
            'com_id': c.id,
            'aut_id': c.user_id,
            'aut_name': c.name,
            'aut_photo': c.profile_photo,
            'pubdate': c.ctime.isoformat(),
            'content': c.content,
            'reply_count': c.reply_count,
            'like_count': c.like_count,
        } for c in comments]
        # 评论总数

        end_id = end_comment.id if end_comment else None

        # 最后一条数据的id
        last_id = comments[-1].id if comments else None

        return {'res': comments_list, 'total': total, 'last_id': last_id, 'end_id': end_id}
