# 获取所有频道信息
from flask_restful import Resource
from sqlalchemy.orm import load_only

from common.models.article import Channel


class AllChannelResource(Resource):
    def get(self):
        channels = Channel.query.all()

        # 将查询到的数据，转换为字典
        channels_list = [channel.to_dict() for channel in channels]

        return {'channel': channels_list}
