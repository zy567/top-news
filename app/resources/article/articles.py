from datetime import datetime
from flask import request
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
import pymysql
from elasticsearch import Elasticsearch

from app import db
from flask import g

from common.models.user import User
from common.models.article import Article, ArticleContent, Collection, Attitude
from common.models.user import Relation
from common.utils.constants import HOME_PRE_PAGE
from common.utils.es import write_es
from sqlalchemy.orm import load_only


# 显示首页 文章信息
class ArticleListResource(Resource):

    def get(self):
        parser = RequestParser()
        parser.add_argument('channel_id', required=True, location='args', type=int)
        parser.add_argument('timestamp', required=True, location='args', type=int)

        args = parser.parse_args()
        channel_id = args.channel_id
        timestamp = args.timestamp

        if channel_id == 0:
            return {'res': [], 'pre_timestamp': 0}

        # 将时间戳转换成年月日时分秒
        date = datetime.fromtimestamp(timestamp * 0.001)
        # 频道对应 & 审核通过 & 发布时间 < timestamp
        data = db.session.query(Article.id, Article.title, Article.user_id, Article.cover, User.name,
                                Article.ctime, Article.comment_count).join(User, Article.user_id == User.id). \
            filter(Article.channel_id == channel_id, Article.status == Article.STATUS.APPROVED, Article.ctime < date). \
            order_by(Article.ctime.desc()).limit(HOME_PRE_PAGE).all()

        # 序列化
        articles = [
            {
                'art_id': item.id,
                'title': item.title,
                'aut_id': item.user_id,
                'pubdate': item.ctime.isoformat(),
                'aut_name': item.name,
                'comm_count': item.comment_count,
                'cover': item.cover
            }
            for item in data]

        pre_timestamp = int(data[-1].ctime.timestamp() * 1000) if data else 0

        return {'res': articles, 'pre_timestamp': pre_timestamp}


# 获取文章详情信息
class ArticleDetaiLResource(Resource):
    # select
    #     nac.article_id,
    #     nab.title,
    #     nab.ctime,
    #     nab.user_id,
    #     ub.name,
    #     ub.profile_photo,
    #     nac.content
    # from news_article_content  nac
    # join news_article_basic nab  on nac.article_id=nab.id
    # join user_basic ub on nab.user_id=ub.id
    # where nac.article_id=1073;

    def get(self, article_id):
        data = db.session.query(Article.id, Article.title, Article.ctime, Article.cover, Article.user_id, User.name,
                                User.profile_photo, ArticleContent.content). \
            join(Article, Article.id == ArticleContent.article_id). \
            join(User, User.id == Article.user_id). \
            filter(ArticleContent.article_id == Article.id).first()
        print(2222, data)

        # 序列化
        articles_dict = {
            'art_id': data.id,
            'title': data.title,
            'aut_id': data.user_id,
            'pubdate': data.ctime.isoformat(),
            'aut_name': data.name,
            'comm_count': data.content,
            'is_followed': False,
            'attitude': -1,
            'is_collected': False
        }
        print(3333, articles_dict)

        # 3. 如果用户是登录的状态。需要获取到当前用户对这个文章的  是否关注了作者、是否收藏了文章、对文章的态度
        # 获取到用户的id
        user_id = g.user_id

        if user_id:  # 如果有user_id 表示 用户已登录
            # 3.1 获取用户是否关注了作者
            # select id from user_relation where user_id=2 and author_id=1 and relation=1;
            is_follow = Relation.query. \
                filter(Relation.user_id == user_id, Relation.author_id == data.user_id,
                       Relation.relation == 1)
            articles_dict['is_followed'] = True if is_follow else False

            # 如果 查询出来的id 是 非空的，表示 已关注。 article_dict 字典中的 is_followed 改为 True

            # 3.2 用户是否收藏了文章
            # select id from news_collection where user_id=2 and article_id=1073 and is_deleted = 0;
            if_collect = Collection.query. \
                options(load_only(Collection.id)). \
                filter(Collection.user_id == user_id, Collection.article_id == article_id,
                       Collection.article_id == False)

            articles_dict['if_collected'] = True if if_collect else False

            # 如果 查询出来的id 是 非空的，表示 已收藏。 article_dict 字典中的is_collected 改为 True

            # 3.3 用户对文章的态度
            # select attitude from news_attitude where user_id=2 and article_id=1073;
            attit = Attitude.query.options(load_only(Attitude.attitude)). \
                filter(Attitude.user_id == user_id, Attitude.article_id == article_id).first()
            print(attit)
            if attit:
                articles_dict['attitude'] = attit.attitude
            # 如果 attitude 有值 将这个值赋值给   article_dict 字典中的 attitude 字段
            # 如果 attitude 没有值 将 -1 没有态度 值赋值给   article_dict 字典中的 attitude 字典

        return articles_dict


class SearchArticleResource(Resource):
    # write_es()
    def get(self):
        # 获取查询字符串里的搜索关键字
        query = request.args.get('query')
        # es 查询
        dict = {
            'query': {
                # 固定 key 的名字
                'multi_match': {
                    'query': query,
                    'fields': ['title']
                }
            }
        }
        # es对象
        es = Elasticsearch("https://elastic:__zpYitPqhzdi2crmfr+@127.0.0.1:9200/", verify_certs=False)
        # 查询
        res = es.search(index='news_article_basic', body=dict, size=5)
        # 格式化校验
        res = res.get('hits').get('hits')
        return res


