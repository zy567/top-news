from celery import Celery

# broker_url= 'redis://127.0.0.1/11'
#
# celery_app = Celery('cle',broker_url=broker_url)
# 创建一个celery的实例对象
celery_app = Celery("toutiao")

# 加载配置
celery_app.config_from_object('celery_tasks.config')

# celery_app.autodiscover_tasks(['celery_task.sms'])

celery_app.autodiscover_tasks(['celery_tasks.sms'])