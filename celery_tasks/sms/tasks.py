from celery_tasks.main import celery_app
from celery_tasks.code import SendSMSCode


# 定义一个任务函数 但是要想任务生效需要定义一个装饰器
@celery_app.task(bind=True, name='send_sms_code', retry_backoff=2)
def send_sms_code(self, mobile, sms_code):
    # 发送短信验证码的方法
    try:
        SendSMSCode.send_sms_code(mobile=mobile, datas=(sms_code, 5))
    except Exception as e:
        raise self.retry(exc=e, max_retries=2)