from ronglian_sms_sdk import SmsSDK
from flask import current_app

# broker_url= 'redis://127.0.0.1/11'
# 自己去封装一个发短信的功能
class SendSMSCode(object):

    def __init__(self):
        """初始化sdk对象"""
        self.sdk = SmsSDK(current_app.config['ACC_ID'], current_app.config['ACC_TOKEN'], current_app.config['APP_ID'])
        self.tid = '1'

    def send_sms_code(self, mobile, datas):
        self.sdk.sendMessage(self.tid, mobile, datas)