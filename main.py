from flask import Flask, jsonify
from flask_restful import Api, Resource

# 1. 创建一个flask app对象
flask_app = Flask(__name__)

# 使用 flask_restful
# 第一步： 初始化一个 Api对象
api = Api(flask_app)


# 第二步，定义 类视图，继承 Resource（类比 Django中 View 类）
class UserResource(Resource):

    def get(self):
        return {"foo": "get"}

    def post(self):
        return {"foo": "post"}


# 第三步 组件添加类视图
api.add_resource(UserResource, "/user")


if __name__ == '__main__':
    flask_app.run(debug=True)