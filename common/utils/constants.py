# 项目中使用到的常量
EXTRA_ENV_COINFIG = 'ENV_CONFIG'  # 额外配置对应的环境变量名

BASE_URL_PRIFIX = '/app'  # 基础URL的前缀


# 短信的有效期 单位：秒
SMS_CODE_EXPIRE = 300

# 每页文章数量
HOME_PRE_PAGE = 10

