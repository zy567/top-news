from flask import request, g
from common.utils.jwt_util import verify_jwt


# 在请求之前，验证 token，验证通过 需要将用户信息保存到 g 变量中。
def get_user_info():
    """验证Token 获取用户id"""
    # 1. 从请求头中获取token
    token = request.headers.get('Authorization')

    # 2. 在g变量中添加一个属性 user_id。默认值为 None
    g.user_id = None

    # 3. 对token进行验证
    if token:
        # 验证
        data = verify_jwt(token)
        if data:
            # 表示 token是没有问题的
            g.user_id = data.get('user_id')
