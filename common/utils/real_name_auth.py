import requests
from flask import current_app


def check_real_name(real_name, id_card):
    """
    验证 实名认证
    :param real_name: 真实姓名
    :param id_card: 身份证号
    :return: bool True 认证成功， False 失败
    """

    # 请求头
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Authorization': 'APPCODE ' + current_app.config['APPCODE']
    }

    # 请求体
    body = {
        'cardNo': id_card,
        'realName': real_name
    }

    # 发送请求
    response = requests.post(current_app.config['AUTH_URL'], headers=headers, data=body)
    print(response.content.decode())

    # 判断是否认证成功
    return response.json().get('isok')
