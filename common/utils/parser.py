import re
import imghdr


def image(value):
    """
    自定义校验上传的文件是否是一个图片类型
    :param value: 要判断的文件的bytes类型数据
    :return:
    """
    try:
        file_type = imghdr.what(value)
    except Exception:
        raise ValueError('Invalid image.')
    else:
        if file_type:
            return value
        else:
            raise ValueError('Invalid image.')


def email(email_str):
    """
    校验邮箱格式
    :param email_str: 被校验的邮箱
    :return: email_str
    """
    if re.match(r'^([A-Za-z0-9_\-\.\u4e00-\u9fa5])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$', email_str):
        return email_str
    else:
        raise ValueError('{} is not a valid email'.format(email_str))


def mobile(mobile_str):
    """
    校验手机号格式
    :param mobile_str: 被校验的手机号
    :return: mobile_str
    """
    if re.match(r'^1[3-9]\d{9}$', mobile_str):
        return mobile_str
    else:
        raise ValueError('{} is not a valid mobile'.format(mobile_str))


def id_card(value):
    """
    校验身份证号格式
    :param value: 被校验的身份证号
    :return: 身份证号
    """
    id_card_pattern = r'(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)'
    if re.match(id_card_pattern, value):
        return value.upper()
    else:
        raise ValueError('Invalid id number.')
