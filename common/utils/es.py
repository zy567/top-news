import pymysql
from elasticsearch import Elasticsearch


# 读取数据
def read_mysql():
    db = pymysql.connect(
        host='localhost',
        port=3306,
        user='root',
        passwd='root',
        db='topnews',
        charset='utf8'
    )
    # 创建游标对象
    cursor = db.cursor()
    # 执行数据库操作
    sql = 'select * from news_article_basic;'

    cursor.execute(sql)

    # 获取查询数据
    result = cursor.fetchall()

    return result


def write_es():
    # es对象
    es = Elasticsearch("https://elastic:__zpYitPqhzdi2crmfr+@127.0.0.1:9200/", verify_certs=False)
    try:
        result = read_mysql()
        for i in result:
            res = {
                'id': i[0],
                'user_id': i[1],
                'channel_id': i[2],
                'title': i[3],
                'cover': i[4],
                'ctime': i[5],
                'status':i[6],
                'comment_count': i[7],
            }
            es.index(index='news_article_basic', document=res, id=i[0])
    except Exception as e:
        print(e)
