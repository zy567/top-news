import jwt
from flask import current_app


def generate_jwt(payload, exp, secret=None):
    """
    生成jwt 的方法
    :param payload: dict 加密的数据。
    :param exp: 过期时间
    :param secret: 密钥
    :return: token
    """
    _payload = {"exp": exp}
    _payload.update(payload)

    if not secret:
        secret = current_app.config['JWT_SECRET']

    # 生成 token
    token = jwt.encode(_payload, secret, algorithm="HS256")

    return token


def verify_jwt(token, secret=None):
    if not secret:
        # 从flask配置文件中读取配置信息
        secret = current_app.config['JWT_SECRET']
    try:
        payload = jwt.decode(token, secret, algorithms="HS256")
    except jwt.PyJWTError:
        payload = None

    return payload
