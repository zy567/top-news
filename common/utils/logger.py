import datetime
import logging
import os

from flask import request, has_request_context


# 自定义格式化类
class RequestFormatter(logging.Formatter):

    def format(self, record: logging.LogRecord) -> str:
        """
        每次生成日志的时候都会调用，该方法主要设置自定义的日志信息
        :param record: 日志信息对象
        :return: 调用父类的方法
        """
        if has_request_context():
            # 添加请求的url地址
            record.url = request.url
            # 获取客户端的ip地址
            record.remote_addr = request.remote_addr
        else:
            # 添加请求的url地址
            record.url = None
            # 获取客户端的ip地址
            record.remote_addr = None

        # 调用父类中的方法
        return super().format(record)


def create_logger():
    """配置Flask中的日志"""
    # 创建flask.app 日志器
    flask_logger = logging.getLogger(__name__)
    if flask_logger.handlers:
        return flask_logger
    # 设置日志的全局等级
    flask_logger.setLevel("DEBUG")
    # 创建控制台日志输出器对象
    console_handler = logging.StreamHandler()
    # 设置格式
    console_formatter = RequestFormatter(fmt='[%(asctime)s] %(remote_addr)s requested %(url)s %(name)s %(levelname)s %(pathname)s %(lineno)d %(message)s')
    console_handler.setFormatter(console_formatter)
    # 日志器添加处理器
    flask_logger.addHandler(console_handler)

    # 获取项目的根目录
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    # 生成日志文件的文件名
    log_file_name = datetime.datetime.now().strftime("%Y-%m-%d-%H")
    file_name = f"{base_dir}/logs/{log_file_name}.log"
    print(file_name)
    # 创建文件输出器对象
    file_handler = logging.FileHandler(filename=file_name, mode='a', encoding='utf8')
    # 给文件日志输出器设置格式
    file_formatter = RequestFormatter(
        fmt='[%(asctime)s] %(remote_addr)s requested %(url)s %(name)s %(levelname)s %(pathname)s %(lineno)d %(message)s')
    file_handler.setFormatter(file_formatter)
    file_handler.setLevel('INFO')
    flask_logger.addHandler(file_handler)

    return flask_logger

