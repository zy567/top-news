from qiniu import Auth, put_data
from flask import current_app


def upload_file(data):
    """
    封装使用七牛云上传图片的方法
    :param data: 上传的图片的 bytes类型数据。
    :return: 图片保存在七牛云中的一个地址 url
    """
    # 需要填写你的 Access Key 和 Secret Key
    access_key = current_app.config["ACCESS_KEY"]
    secret_key = current_app.config["SECRET_KEY"]
    # 构建鉴权对象
    q = Auth(access_key, secret_key)
    # 要上传的空间
    bucket_name = current_app.config["BUCKET_NAME"]
    # 上传后保存的文件名
    # 设置为None之后，七牛云会将我们上传的文件 进行 hash计算，得到的结果作为文件名
    key = None
    # 生成上传 Token，可以指定过期时间等
    token = q.upload_token(bucket_name, key, 3600)

    ret, info = put_data(token, key, data)
    print(ret, info)
    if info.status_code == 200:  # 上传成功
        return current_app.config['QINIU_DOMAIN'] + ret.get('key')
    else:
        raise Exception(info.error)


