from werkzeug.routing import BaseConverter
from flask import Flask


# 1. 创建类，继承 BaseConverter
class MobileConverter(BaseConverter):
    # 定义一个 类属性
    regex = r'1[3-9]\d{9}'


# 定义一个函数，这个函数一旦被调用，就会将我们的 转换器进行定义
def register_converters(app: Flask):
    app.url_map.converters["mobile"] = MobileConverter
