# 发送短信
from ronglian_sms_sdk import SmsSDK
from flask import current_app


class SendSMSCode(object):

    def __init__(self):
        # 创建sdk对象
        self.sdk = SmsSDK(current_app.config['ACC_ID'], current_app.config['ACC_TOKEN'], current_app.config['APP_ID'])
        self.tid = "1"   # 短信模板

    def send_sms_code(self, mobile, datas):
        """
        实现发送短信验证码的方法
        :param mobile: 手机号
        :param datas: 元组，(验证码, 时间)
        """
        self.sdk.sendMessage(self.tid, mobile=mobile, datas=datas)
