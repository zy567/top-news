from flask import g
from functools import wraps


def login_required(f):  # f 指的是 被装饰的视图函数
    """装饰器： 装饰视图函数，在访问视图之前，对登录去做验证"""

    # 不会改变被装饰函数的名字
    @wraps(f)
    def wrapper(*args, **kwargs):

        # 在钩子函数中，我们已经对 g变量中的user_id进行了赋值，（None、当前登录用户的id）
        if g.user_id:
            # 用户是登录的状态
            return f(*args, **kwargs)
        else:
            # 用户未登录
            return {'message': 'Invalid Token', 'data': None}

    return wrapper
