# 发送短信
from ronglian_sms_sdk import SmsSDK
from . import constants


class SendSMSCode(object):
    def __init__(self):
        # 创建sdk对象
        self.sdk = SmsSDK(constants.ACC_ID, constants.ACC_TOKEN, constants.APP_ID)
        self.tid = '1'

    def send_sms_code(self, mobile, datas):
        """
        发送短信验证码的方法
        :param mobile: 手机号
        :param datas:  元组 （验证码， 时间）
        """
        self.sdk.sendMessage(self.tid, mobile=mobile, datas=datas)