from flask import request, g
from common.utils.jwt_util import verify_jwt


def get_user_info():
    token = request.headers.get('Authorization')

    # 2. 在g变量中添加一个属性。user_id。默认值为 None
    g.user_id = None

    # 3. 对token进行验证
    if token:
        # 如果前端传递了token，我们再去验证  {"userid": 1, "exp": xxxxxx}
        data = verify_jwt(token)
        if data:
            # 表示 token是没有问题的
            g.user_id = data.get("user_id")
