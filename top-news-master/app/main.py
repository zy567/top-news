from app import create_app
from flask import jsonify

# 创建flask对象
app = create_app('dev')


# 测试接口
@app.route('/')
def index():
    return jsonify({'username': '张三', 'mobile': '18771552515'})

