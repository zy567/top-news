# 项目的配置文件
class DefaultConfig:
    # 默认的配置信息
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@127.0.0.1:3306/topnews'  # 连接地址
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 是否追踪数据变化
    SQLALCHEMY_ECHO = False  # 是否打印底层执行的SQL

    # redis配置
    REDIS_HOST = '127.0.0.1'  # ip
    REDIS_PORT = 6379  # 端口

# 项目中使用的加密的密钥
    JWT_SECRET = 't!29-9k&o6ph&gv@pr_-9i%^*0yne4hev*e5wl#)=l)#l4yglo'
    # jwt密钥的过期时间
    JWT_EXPIRE_DAYS = 14

# key:value  key 配置类的别名  value 具体的配置类
config_dict = {
    "dev": DefaultConfig
}
