import re
import random

from flask import request, current_app
from datetime import datetime, timedelta
from flask_restful import Resource

from app import redis_client, db
from common.utils.send_sms_code import SendSMSCode
from common.models.user import User
from common.utils.jwt_util import generate_jwt


# TODO: 发送短信验证码
class SMSCodeResource(Resource):
    def get(self, mobile):
        # 在此函数中去实现发送短信验证码的业务逻辑
        # 1. 生成一个随机的六位的数字的验证码
        code = '%06d' % random.randint(0, 999999)
        # 2. 生成的短信验证码 保存到redis（登录的时候：读取redis中存储的验证码 和用户输入的验证码 对比）
        redis_client.set(mobile, code)
        # 3. 调用发送短信的方法，给用户去发送短信
        SendSMSCode().send_sms_code(mobile, datas=(code, '1'))
        return {"sms_code": code}


# TODO: 实现登录和注册的视图
class LoginResource(Resource):

    def post(self):
        # 1. 获取前端传递来的数据 json数据
        mobile = request.json.get('mobile')
        code = request.json.get('code')
        print(code)

        # 2. 数据校验  判断数据完整性
        if not all([mobile, code]):
            return {'msg': '缺失参数, 参数不能为空'}
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return {'msg': '手机号格式错误'}

        # 验证码
        redis_code = redis_client.get(mobile)
        if not redis_code or code != redis_code:
            return {"msg": '验证码错误'}

        # 3. 处理： 根据手机号 从数据库中查询一下，这个手机号是否存在
        # 3.1 手机号是存在的：验证用户输入的短信验证码是否正确，如果正确，登录成功，状态保持（生成jwt） 验证码错误：登录失败
        user_mobile = User.query.filter_by(mobile=mobile).first()
        # 3.2 手机号是不存在：验证用户输入的短信验证码是否正确，如果正确，将用户的信息保存到数据库（注册） 登录 状态保持。
        if not user_mobile:
            # 手机号保存到user模型类中
            user = User(mobile=mobile)
            db.session.add(user)
            db.session.commit()

        # 获取jwt token
        token = generate_jwt({'mobile': mobile},
                             datetime.utcnow() + timedelta(days=current_app.config['JWT_EXPIRE_DAYS'])).decode()

        return {'token': token}

        # 3.2 手机号是不存在：验证用户输入的短信验证码是否正确，如果正确，将用户的信息保存到数据库（注册） 登录 状态保持。
