from flask import g
from flask_restful import Resource
from sqlalchemy.orm import load_only

from common.utils.decorators import login_required
from common.models.user import User


class CurrentUserResource(Resource):
    """实现返回用户个人中心 用户信息"""
    # 在类视图中要给方法添加装饰器，使用类属性  method_decorators 属性
    # method_decorators
    # 第一种值 是一个列表.  method_decorators = [装饰器函数名1, 装饰器函数名2]   表示 对类视图中的每一个请求的方法都会加上装饰器、
    # method_decorators = [login_required]
    # 第二种值  是一个字典  method_decorators = {"请求方式": [装饰器的函数名]}
    method_decorators = {"get": [login_required]}  # 表示 只给 get请求 添加了 装饰器

    def get(self):
        # 只要能够执行到这个get函数里面，表示 用户是登录的状态了
        # 1. 获取登录用户的id
        user_id = g.user_id
        # 2. 根据用户id 从User模型类中去查询到对应的用户对象
        user = User.query.options(load_only(User.id, User.name, User.profile_photo, User.introduction, User.article_count, User.following_count, User.fans_count)).filter(User.id == user_id).first()
        # 最终实现的： 返回当前登录用户的 信息
        return user.to_dict()
