import sys
from os.path import dirname, abspath
from flask import Flask
from app.settings.config import config_dict
from common.utils.constants import EXTRA_ENV_COINFIG
from flask_sqlalchemy import SQLAlchemy
from redis import StrictRedis
from flask_migrate import Migrate
from common.utils.middlewares import get_user_info



# 将common路径添加到系统导包路径中
BASE_DIR = dirname(dirname(abspath(__file__)))

#
sys.path.insert(0, BASE_DIR + '/common')

# 创建一个SQLAlchemy 对象
db = SQLAlchemy()

# 创建一个连接
redis_client = None  # type: StrictRedis



# 注册各种组件的方法
def register_extensions(app: Flask):
    db.init_app(app)

    # redis数据库的初始化 创建连接对象
    global redis_client
    # host 连接的redis 的ip地址
    # port 连接的redis 的端口号
    # decode_responses=True 默认情况下，使用python读取redis中的数据的时候，获取到的数据类型是 bytes类型，加上这个参数之后，自动解码。
    redis_client = StrictRedis(host=app.config['REDIS_HOST'], port=app.config['REDIS_PORT'], db=0,
                               decode_responses=True)

    # 添加自定义路由转换器
    from common.utils.converters import register_converters
    register_converters(app)

    # 初始化数据迁移组件
    Migrate(app, db)

    # 自定义的钩子函数
    app.before_request(get_user_info)

    # 导入初始化模型类
    from common.models import user




def create_flask_app(type):
    """
    创建Flask应用对象 app对象
    :param type: 配置类型（开发配置环境、线上的配置环境）
    :return: Flask app对象
    """
    # 1. 创建Flask对象
    flask_app = Flask(__name__)

    # 2. 根据配置的类型，获取到配置的类
    config_class = config_dict[type]

    # 3. 从配置类中加载项目的配置
    flask_app.config.from_object(config_class)
    # 再从额外配置中加载配置
    flask_app.config.from_envvar(EXTRA_ENV_COINFIG, silent=True)

    # 返回创建好的flask对象
    return flask_app


# 注册蓝图
def register_bp(app: Flask):
    from app.resoureces.user import user_bp

    # 注册用户相关的蓝图
    app.register_blueprint(user_bp)



def create_app(type):
    """
    获取flask app 对象 和创建应用的组件
    :param type: 配置类型（开发配置环境、线上的配置环境）
    :return: flask app对象
    """
    app = create_flask_app(type)
    # 初始化组件
    register_extensions(app)

    # 调用注册蓝图的函数， 完成蓝图注册
    register_bp(app)

    return app
